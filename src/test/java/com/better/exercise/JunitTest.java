package com.better.exercise;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.batch.job.enabled=false", "logging.level.root=info"})
public class JunitTest {

    @Autowired
    @Qualifier("sampleJobLauncherTestUtils")
    private JobLauncherTestUtils sampleJobLauncherTestUtils;

    @Test
    public void sampleJobTest() throws Exception {
        JobExecution jobExecution = sampleJobLauncherTestUtils.launchJob();
        Assert.assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
    }
}
