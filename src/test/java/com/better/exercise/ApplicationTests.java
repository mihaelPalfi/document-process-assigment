package com.better.exercise;

import com.better.exercise.domain.Doctor;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import static org.junit.Assert.assertNotNull;

@SpringBootTest
class ApplicationTests {

    private static final Logger log = LoggerFactory.getLogger(ApplicationTests.class);

    @Value("classpath:data/data.xml")
    private Resource resourceXML;

    @Test
    void contextLoads() {
    }

    @Test
    public void readAndParseXMLFile() throws Exception {

        StaxEventItemReader<Doctor> xmlFileReader = new StaxEventItemReader<>();
        xmlFileReader.setResource(resourceXML);
        xmlFileReader.setFragmentRootElementName("doctor");
        xmlFileReader.open(new ExecutionContext());

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(Doctor.class);
        xmlFileReader.setUnmarshaller(marshaller);
        Doctor doctor = xmlFileReader.read();
        assertNotNull(doctor);
    }




}
