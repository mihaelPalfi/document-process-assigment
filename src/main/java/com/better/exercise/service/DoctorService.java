package com.better.exercise.service;

import com.better.exercise.dto.DiseaseDTO;
import com.better.exercise.dto.DoctorDTO;
import com.better.exercise.dto.PatientDTO;
import com.better.exercise.respository.DiseaseRepository;
import com.better.exercise.respository.DoctorRepository;
import com.better.exercise.respository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorService {

    private static final Logger log = LoggerFactory.getLogger(DoctorService.class);

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private DiseaseRepository dieaseRepository;

    public DoctorDTO save(DoctorDTO doctor) {
        return doctorRepository.save(doctor);
    }

    public DoctorDTO getDoctorById(Long id) {
        return doctorRepository.getOne(id);
    }

    public List<DoctorDTO> getDoctors() {
        return doctorRepository.findAll();
    }

    public List<PatientDTO> getPatients() {
        return patientRepository.findAll();
    }

    public List<PatientDTO> getPatientBylastName(String lastName) {
        return patientRepository.findByLastName(lastName);
    }

    public List<DiseaseDTO> getAllDiseases() {
        return dieaseRepository.findAll();
    }
}
