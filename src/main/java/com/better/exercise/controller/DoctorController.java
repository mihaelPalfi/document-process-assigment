package com.better.exercise.controller;

import com.better.exercise.dto.DiseaseDTO;
import com.better.exercise.dto.DoctorDTO;
import com.better.exercise.dto.PatientDTO;
import com.better.exercise.service.DoctorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/public/data")
public class DoctorController {

    private static final Logger log = LoggerFactory.getLogger(DoctorController.class);

    @Autowired
    private DoctorService doctorService;

    @GetMapping(value = "/doctors")
    public List<DoctorDTO> getDoctors() {
        return doctorService.getDoctors();
    }

    @GetMapping(value = "/doctors/{id}")
    public DoctorDTO getDoctor(Long Id) {
        return doctorService.getDoctorById(Id);
    }

    @GetMapping(value = "/patients")
    public List<PatientDTO> getPatients() {
        return doctorService.getPatients();
    }

    @GetMapping(value = "/patients/{lastName}")
    public List<PatientDTO> getPatientByLastName(String lastName) {
        return doctorService.getPatientBylastName(lastName);
    }

    @GetMapping(value = "/diseases")
    public List<DiseaseDTO> getAllDiseases() {
        return doctorService.getAllDiseases();
    }

}


