package com.better.exercise.logic;

import com.better.exercise.domain.Doctor;
import com.better.exercise.dto.DoctorDTO;
import com.better.exercise.respository.DoctorRepository;
import com.better.exercise.respository.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.UnexpectedRollbackException;

import java.util.List;


public class DataItemWriter implements ItemWriter<Doctor> {

    private static final Logger log = LoggerFactory.getLogger(DataItemWriter.class);

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private ReportRepository reportRepository;

    private StepExecution stepExecution;
    private String fileName;
    private Object doctorId;


    public DataItemWriter(String fileName, StepExecution stepExecution) {
        this.fileName = fileName;
        this.stepExecution = stepExecution;
    }

    @Override
    public void write(List<? extends Doctor> data) {

        log.info("IN WRITER");
        try {
            for (Doctor doctor : data) {
                DoctorDTO dto = doctor.createDTO();

                // save to database
                dto.setImportFilePath(fileName);
                DoctorDTO result = doctorRepository.saveAndFlush(dto);

                setDoctorId(result.getId());
                log.info("Data Saved : {}", result);
            }
        } catch (UnexpectedRollbackException ex){

        }

        //this.stepExecution.getJobExecution().getExecutionContext().put("doctorId", getDoctorId());
        //this.stepExecution.getJobExecution().getExecutionContext().put("fileName", getFileName());

        // set doctor id and filename to step context for report saving
        this.stepExecution.getExecutionContext().put("doctorId", getDoctorId());
        this.stepExecution.getExecutionContext().put("fileName", getFileName());
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public Object getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Object doctorId) {
        this.doctorId = doctorId;
    }


}
