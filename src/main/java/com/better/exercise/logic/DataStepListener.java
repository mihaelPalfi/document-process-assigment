package com.better.exercise.logic;

import com.better.exercise.dto.ReportDTO;
import com.better.exercise.respository.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class DataStepListener implements StepExecutionListener {

    private static final Logger log = LoggerFactory.getLogger(DataStepListener.class);

    @Autowired
    private ReportRepository repository;

    @Override
    public void beforeStep(StepExecution stepExecution) {
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        log.info("AFTER STEP FINISHED! Step status {}", stepExecution);

        for (StepExecution exec : stepExecution.getJobExecution().getStepExecutions()) {
            Long doctorId = null;
            String fileName = null;

            if (exec.getExecutionContext().get("doctorId") != null) {
                doctorId = exec.getExecutionContext().getLong("doctorId");
            }
            if (exec.getExecutionContext().get("fileName") != null) {
                fileName = exec.getExecutionContext().getString("fileName");
            }

            if(fileName != null && !fileName.isEmpty() && doctorId != null){
                log.info("Time to verify the results and create a report.");

                ReportDTO reportDTO = new ReportDTO();
                reportDTO.setJobId(stepExecution.getId());
                reportDTO.setStatus(stepExecution.getStatus());
                reportDTO.setDoctorId(doctorId);
                reportDTO.setDocumentSource(fileName);
                reportDTO.setItemsProcessed(stepExecution.getCommitCount());
                Date startDate = stepExecution.getJobExecution().getStartTime();
                Date endDate = new Date();

                reportDTO.setStartTime(startDate);
                reportDTO.setEndTime(endDate);
                reportDTO.setExecutionTime(endDate.getTime() - startDate.getTime());
                reportDTO.setErrorMessage(stepExecution.getFailureExceptions().toString());
                repository.save(reportDTO);
            }
        }

        List<Throwable> failureExceptions = stepExecution.getFailureExceptions();

        if (!failureExceptions.isEmpty() && failureExceptions.size() > 0) {
            return ExitStatus.UNKNOWN;
        }
        if (stepExecution.getReadCount() == 0) {
            return ExitStatus.UNKNOWN;
        }

        return stepExecution.getExitStatus();
    }

}
