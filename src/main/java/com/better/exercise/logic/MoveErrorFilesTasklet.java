package com.better.exercise.logic;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Collection;


public class MoveErrorFilesTasklet implements Tasklet, InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(MoveErrorFilesTasklet.class);

	private String resourcesPath;
	private Resource[] resources;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		Collection<StepExecution> stepExecutions = chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getStepExecutions();
		for (StepExecution stepExecution : stepExecutions) {
			if (stepExecution.getExecutionContext().containsKey("fileName")) {
				String file = stepExecution.getExecutionContext().getString("fileName");
				String path = file.replace("file:/", "");
				String[] filename = file.split("/");

				File destFile = new File(resourcesPath, filename[6]);
				if(destFile.exists()) {
					destFile.delete();
				}

				FileUtils.moveFile(FileUtils.getFile(path),
						FileUtils.getFile(resourcesPath + filename[6]));
			}
		}

		return RepeatStatus.FINISHED;

	}

	public String getResourcesPath() {
		return resourcesPath;
	}
	public void setResourcesPath(String resourcesPath) {
		this.resourcesPath = resourcesPath;
	}

	public void setResources(Resource[] resources) {
		this.resources = resources;
	}

	public void afterPropertiesSet() throws Exception {
		Assert.notNull(resources, "directory must be set");
	}

}