package com.better.exercise.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Component
public class DataJobListener extends JobExecutionListenerSupport {
    private static final Logger log = LoggerFactory.getLogger(DataJobListener.class);

    @Override
    public void beforeJob(JobExecution jobExecution) {
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("AFTER JOB LISTENER");
    }
}
