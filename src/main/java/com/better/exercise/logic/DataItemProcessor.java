package com.better.exercise.logic;

import com.better.exercise.domain.Doctor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class DataItemProcessor implements ItemProcessor<Doctor, Doctor> {

    private static final Logger log = LoggerFactory.getLogger(DataItemProcessor.class);


    @Override
    public Doctor process(Doctor doctor) throws Exception {
        log.info("IN PROCESSOR");
        return doctor;
    }

}
