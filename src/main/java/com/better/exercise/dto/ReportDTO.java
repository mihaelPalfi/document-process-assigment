package com.better.exercise.dto;

import org.springframework.batch.core.BatchStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "job_report")
public class ReportDTO {

    @Id
    @Column(name = "JOB_ID")
    private Long jobId;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private BatchStatus status;

    @Column(name = "ITEMS_PROCESSED")
    private Integer itemsProcessed;

    @Column(name = "START_TIME", updatable = false)
    private Date startTime;

    @Column(name = "END_TIME", updatable = false)
    private Date endTime;

    @Column(name = "EXECUTION_TIME", updatable = false)
    private Long executionTime;

    @Column(name = "DOCTOR_ID")
    private Long doctorId;

    @Column(name = "ERROR_MESSAGE", length = 1024)
    private String errorMessage;

    @Column(name = "DOCUMENT_SOURCE")
    private String documentSource;


    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public BatchStatus getStatus() {
        return status;
    }

    public void setStatus(BatchStatus status) {
        this.status = status;
    }

    public Integer getItemsProcessed() {
        return itemsProcessed;
    }

    public void setItemsProcessed(Integer itemsProcessed) {
        this.itemsProcessed = itemsProcessed;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getDocumentSource() {
        return documentSource;
    }

    public void setDocumentSource(String documentSource) {
        this.documentSource = documentSource;
    }

    @Override
    public String toString() {
        return "ReportDTO{" +
                "jobId=" + jobId +
                ", status=" + status +
                ", itemsProcessed=" + itemsProcessed +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", executionTime=" + executionTime +
                ", doctorId=" + doctorId +
                ", errorMessage='" + errorMessage + '\'' +
                ", documentSource='" + documentSource + '\'' +
                '}';
    }
}
