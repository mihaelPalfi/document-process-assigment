package com.better.exercise.dto;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "patient")
public class PatientDTO {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<DiseaseDTO> diseases;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<DiseaseDTO> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<DiseaseDTO> diseases) {
        this.diseases = diseases;
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diseases=" + diseases +
                '}';
    }
}
