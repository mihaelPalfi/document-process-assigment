package com.better.exercise.dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "doctor")
public class DoctorDTO extends Audit {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "department")
    private String department;

    @Column(name = "importFilePath")
    private String importFilePath;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PatientDTO> patients = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getImportFilePath() {
        return importFilePath;
    }

    public void setImportFilePath(String importFilePath) {
        this.importFilePath = importFilePath;
    }

    public List<PatientDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDTO> patients) {
        this.patients = patients;
    }

    @Override
    public String toString() {
        return "DoctorDTO{" +
                "id=" + id +
                ", department='" + department + '\'' +
                ", patients=" + patients +
                '}';
    }
}
