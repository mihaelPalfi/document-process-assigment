package com.better.exercise.dto;

import javax.persistence.*;

@Entity
@Table(name = "disease")
public class DiseaseDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "disease")
    private String disease;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    @Override
    public String toString() {
        return "DiseaseDTO{" +
                "id=" + id +
                ", disease='" + disease + '\'' +
                '}';
    }
}
