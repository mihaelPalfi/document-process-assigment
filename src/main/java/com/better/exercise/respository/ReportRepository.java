package com.better.exercise.respository;

import com.better.exercise.dto.ReportDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<ReportDTO, Long> {

}
