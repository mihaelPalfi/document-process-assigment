package com.better.exercise.respository;

import com.better.exercise.dto.DiseaseDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiseaseRepository extends JpaRepository<DiseaseDTO, Long> {

}
