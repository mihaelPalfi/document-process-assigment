package com.better.exercise.respository;

import com.better.exercise.dto.PatientDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<PatientDTO, Long> {

    List<PatientDTO> findByLastName(String lastName);

}
