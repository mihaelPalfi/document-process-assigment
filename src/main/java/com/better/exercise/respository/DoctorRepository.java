package com.better.exercise.respository;

import com.better.exercise.dto.DoctorDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends JpaRepository<DoctorDTO, Long> {

}
