package com.better.exercise.config;

import com.better.exercise.domain.Doctor;
import com.better.exercise.logic.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.partition.support.MultiResourcePartitioner;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.task.TaskExecutor;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

@Configuration
@EnableBatchProcessing
public class BatchJobConfig {

    private Logger log = LoggerFactory.getLogger(BatchJobConfig.class);

    @Value("${files.path}")
    private String inputPath;

    @Value("${files.error.path}")
    private String errorPath;

    @Value("${files.success.path}")
    private String sucessPath;

    @Value("${files.type}")
    private String fileType;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job;

    @Scheduled(cron = "${spring.batch.job.cron.expression}")
    public void schedule() {
        try {
            JobParameters jobParameters = new JobParametersBuilder().addDate("launchDate", new Date())
                    .toJobParameters();
            jobLauncher.run(job, jobParameters);
            log.info("Batch is Running...");
        } catch (Exception e) {
        }
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(20);
        taskExecutor.setCorePoolSize(20);
        taskExecutor.setQueueCapacity(20);
        taskExecutor.setAllowCoreThreadTimeOut(true);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

    @Bean
    @StepScope
    public Partitioner partitioner() {
        log.info("IN PARTITIONER");

        MultiResourcePartitioner petitioner = new MultiResourcePartitioner();
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = null;
        try {
            resources = resolver.getResources("file:" + inputPath + fileType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        petitioner.setResources(resources);
        petitioner.partition(10);
        return petitioner;
    }

    @Bean
    @StepScope
    public StaxEventItemReader<Doctor> reader(@Value("#{stepExecutionContext['fileName']}") String filename) throws MalformedURLException {
        log.info("IN READER");

        // Create xml reader and set parsing marshaller for xml files
        StaxEventItemReader<Doctor> reader = new StaxEventItemReader<>();
        reader.setFragmentRootElementName("doctor");
        reader.setResource(new UrlResource(filename));

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(Doctor.class);
        reader.setUnmarshaller(marshaller);

        return reader;
    }

    /*
        @Bean
        public MultiResourceItemReader<Doctor> multiResourceItemReader() throws IOException {
            MultiResourceItemReader<Doctor> resourceItemReader = new MultiResourceItemReader<Doctor>();
            resourceItemReader.setResources(inputResources);
            resourceItemReader.setDelegate(reader());
            return resourceItemReader;
        }
    */

    @Bean
    @StepScope
    public ItemProcessor<Doctor, Doctor> processor() {
        return new DataItemProcessor();
    }

    @Bean
    @StepScope
    public ItemWriter<Doctor> writer(@Value("#{stepExecutionContext['fileName']}") String filename,
                                     @Value("#{stepExecution}") StepExecution stepExecution) {
        return new DataItemWriter(filename, stepExecution);
    }


    protected MoveSuccessFilesTasklet moveSuccessFiles() {
        MoveSuccessFilesTasklet moveSuccessFilesTasklet = new MoveSuccessFilesTasklet();
        try {
            moveSuccessFilesTasklet.setResourcesPath(sucessPath);
            moveSuccessFilesTasklet.setResources(new PathMatchingResourcePatternResolver().getResources("file:" + sucessPath + fileType));
        } catch (IOException e) {

        }
        return moveSuccessFilesTasklet;
    }

    protected MoveErrorFilesTasklet moveErrorFiles() {
        MoveErrorFilesTasklet moveFilesTasklet = new MoveErrorFilesTasklet();
        try {
            moveFilesTasklet.setResourcesPath(errorPath);
            moveFilesTasklet.setResources(new PathMatchingResourcePatternResolver().getResources("file:" + errorPath + fileType));
        } catch (IOException e) {

        }
        return moveFilesTasklet;
    }

    @Bean
    public Job job(
            JobBuilderFactory jobBuilderFactory,
            StepBuilderFactory stepBuilderFactory,
            DataStepListener dataStepListener,
            DataJobListener dataJobListener,
            StaxEventItemReader<Doctor> reader,
            ItemProcessor<Doctor, Doctor> processor,
            ItemWriter<Doctor> writer
    ) {

        // Create step for reading/processing/writing
        Step stepImportData = stepBuilderFactory.get("ETL-import-data")
                .<Doctor, Doctor>chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();

        // Create step for multi file loading
        Step multiFileStep = stepBuilderFactory.get("ETL-file-load")
                .partitioner("multiFileStep", partitioner())
                .step(stepImportData)
                .taskExecutor(taskExecutor())
                .listener(dataStepListener)
                .build();

        // Create step for archiving files
        Step moveSuccessTasklet = stepBuilderFactory.get("moveSuccessFiles").tasklet(moveSuccessFiles()).build();
        Step moveErrorTasklet = stepBuilderFactory.get("moveErrorFiles").tasklet(moveErrorFiles()).build();

        // Create job with necessary steps execution
        return jobBuilderFactory.get("ETL-import")
                .incrementer(new RunIdIncrementer())
                .listener(dataJobListener)
                .start(multiFileStep).on("COMPLETED").to(moveSuccessTasklet)
                .from(multiFileStep).on("UNKNOWN").to(moveErrorTasklet)
                .end()
                .build();
    }


}
