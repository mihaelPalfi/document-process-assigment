package com.better.exercise.domain;

import com.better.exercise.dto.DoctorDTO;
import com.better.exercise.dto.PatientDTO;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "doctor")
public class Doctor {

    @XmlAttribute
    private Long id;

    @XmlAttribute
    private String department;

    @XmlElementWrapper(name = "patients")
    @XmlElements({@XmlElement(name = "patient", type = Patient.class)})
    private List<Patient> patients = new ArrayList();

    public void setId(Long id) {
        this.id = id;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", department='" + department + '\'' +
                ", patients=" + patients +
                '}';
    }

    public DoctorDTO createDTO() {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setId(this.id);
        doctorDTO.setDepartment(this.department);

        List<PatientDTO> patientList = new ArrayList<>();
        for (Patient patient : this.patients) {
            patientList.add(patient.createDTO());
        }
        doctorDTO.setPatients(patientList);

        return doctorDTO;

    }
}
