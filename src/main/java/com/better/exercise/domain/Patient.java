package com.better.exercise.domain;

import com.better.exercise.dto.DiseaseDTO;
import com.better.exercise.dto.PatientDTO;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "patient")
public class Patient {

    @XmlElement
    private Long id;

    @XmlElement(name = "first_name")
    @Column(name = "firstName")
    private String firstName;

    @XmlElement(name = "last_name")
    private String lastName;

    @XmlElementWrapper(name = "diseases")
    @XmlElements({@XmlElement(name = "disease", type = String.class)})
    private List<String> diseases = new ArrayList<>();

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDiseases(List<String> diseases) {
        this.diseases = diseases;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diseases=" + diseases +
                '}';
    }

    public PatientDTO createDTO() {
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setId(this.id);
        patientDTO.setFirstName(this.firstName);
        patientDTO.setLastName(this.lastName);

        List<DiseaseDTO> diseaseList = new ArrayList<>();
        for (String disease : diseases) {
            DiseaseDTO diseaseDTO = new DiseaseDTO();
            diseaseDTO.setDisease(disease);
            diseaseList.add(diseaseDTO);
        }
        patientDTO.setDiseases(diseaseList);
        return patientDTO;
    }

}
