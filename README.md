# Assignment for processing data

Create an application that processes XML or JSON documents. Application gets input documents from the “input” directory or from a user http POST web service call. Application should process “input” folder every 90 seconds.
Elements must be inserted into database and validated. Create tables that correspond to the example provided at the end of the page.

After successful execution:

The document should be stored into the “out” directory or if something fails in the “error” directory
Some info on the process should be stored in the “document_report” table:
- execution time
- doctor id
- error – if any error happens during execution
- document source (folder or http request)
Project should use latest Java (or Kotlin), Spring Framework, ORM (Hibernate/MyBatis/JOOQ ...).
For build tool you can use Maven or Gradle. Use Spring Boot or Dropwizard. Structure code into 3 layers (controller, logic, DAO/repository).
You can document http service
if you want with Swagger or Spring REST Docs and also expose querying doctor, patients, and list of diseases if you want.
Application should have 2 profiles for testing and development. For testing use in-memory database H2 or HSQL and for development use Postgres.


# Project configuration

- Configurations saved in application-environment.properties files.Change file folder paths!
- Change environment with clean spring-boot:run -Dspring-boot.run.profiles=environment. Default environment is local.
- swagger UI reachable on http://localhost:8080/swagger-ui.html#/doctor-controller
- Database reachble when running with local on http://localhost:8080/h2-console/

# Technologies used
- maven
- spring boot
- spring batch
- jobs
- job scheduler
- swagger for ui
- JPA and hibernate
